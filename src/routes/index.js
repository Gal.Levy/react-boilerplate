import { Home, Todos } from "../views/pages"

const routes = [
    {
        path: "/",
        component: Home,
        exact: true,
    },
    {
        path: "/todos",
        component: Todos,
        exact: true,
    }
]

export default routes