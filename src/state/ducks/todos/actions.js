import {
    FETCH_TODOS,
    FETCH_TODOS_FAILURE,
    FETCH_TODOS_SUCCESS
} from "./types"

export function fetchTodos() { 
    return {
        type: FETCH_TODOS,
    }
}

export function fetchTodosSuccess(todos) {
    return {
        type: FETCH_TODOS_SUCCESS,
        payload: todos
    }
}

export function fetchTodosFailure(error) {
    return {
        type: FETCH_TODOS_FAILURE,
        payload: error
    }
}

export function fetchTodosList() {
    return (dispatch) => {
        return dispatch(fetchTodosSuccess([
            "todo1",
            "todo2",
            "todo3"
        ]))
    }
}