import reducer from "./reducers"
import { fetchTodosList } from "./actions"

export {
    fetchTodosList
}

export default reducer