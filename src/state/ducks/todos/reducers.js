import {
    FETCH_TODOS,
    FETCH_TODOS_SUCCESS,
    FETCH_TODOS_FAILURE
} from "./types"

const initialState = {
    list: [],
    loading: false,
    error: false
}

function todosReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_TODOS:
            return { ...state, loading: true }
        case FETCH_TODOS_SUCCESS:
            return { ...state, loading: false, error: false, list: action.payload }
        case FETCH_TODOS_FAILURE:
            return { ...state, loading: false, error: true }
        default:
            return state
    }
}

export default todosReducer