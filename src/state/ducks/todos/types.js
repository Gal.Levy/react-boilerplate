export const FETCH_TODOS = "todolist/FETCH_TODOS"
export const FETCH_TODOS_SUCCESS = "todolist/FETCH_TODOS_SUCCESS"
export const FETCH_TODOS_FAILURE = "todolist/FETCH_TODOS_FAILURE"
