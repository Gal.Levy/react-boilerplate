import React, { Component } from 'react'
import { connect } from "react-redux"
import { fetchTodosList } from "../../../state/ducks/todos"

class TodosList extends Component {
    componentDidMount() {
        this.props.fetchTodosList()
    }

    render() {
        const { loading, error, list } = this.props

        if (loading) {
            return "Loading..."
        }

        if (error) {
            return "Fetch error"
        }

        return (
            <ul>
                {list.map(todo => <div>{todo}</div>)}
            </ul>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.todos.list,
        loading: state.todos.loading,
        error: state.todos.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTodosList: () => dispatch(fetchTodosList())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosList)
