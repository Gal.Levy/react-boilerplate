import React, { Component } from "react"
import { Link, Route, Redirect, Switch, BrowserRouter } from "react-router-dom"
import routes from "../../../routes"
import "./style.css"

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <header>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/todos">Todos</Link></li>
                        </ul>
                    </header>

                    <Switch>
                        {routes.map(route => (
                            <Route key={route.path} {...route} />
                        ))}
                        <Redirect to='/' />
                    </Switch>

                    <footer>
                        I`m the footer, I am on every page.
                    </footer>
                </div>
            </BrowserRouter>
        )
    }
}

export default App
