import React, { Component } from "react"
import TodosList from "../../containers/TodosList";

class Todos extends Component {
    render() {
        return (
            <div>
                <div>Todos List:</div>
                <TodosList />
            </div>
        )
    }
}

export default Todos
